package com.mikejohn.weather;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.mikejohn.weather.common.LoadingDialog;
import com.mikejohn.weather.common.LoadingView;
import com.mikejohn.weather.common.SimpleDividerItemDecoration;
import com.mikejohn.weather.connection.App;
import com.mikejohn.weather.model.City;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, CitiesAdapter.OnItemClick{

    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CitiesAdapter mAdapter;
    private LoadingView mLoadingView;

    private List<City> cities;
    public final String TAG = "MyLog";
    private List<City> loadedCity = new ArrayList<>();
    protected static final String CITY_NAME_KEY = "city_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = findViewById(R.id.toolbar);
        mRecyclerView = findViewById(R.id.recyclerView);
        mSwipeRefreshLayout = findViewById(R.id.swipeContainer);
        setSupportActionBar(mToolbar);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this, false));
        mAdapter = new CitiesAdapter(getInitialCities(), this);
        mRecyclerView.setAdapter(mAdapter);
        mLoadingView = LoadingDialog.view(getSupportFragmentManager());

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);

        cities = getInitialCities();
        load(cities, false);
    }

    @Override
    public void onRefresh() {
        load(cities, true); //запускаем метод загрузки данных
        new Handler().postDelayed(new Runnable() {   //создаем новый поток, чтобы анимация крутилась в фоне
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);  //прячем виджет после завершения запроса load
            }
        }, 4000);
    }

    @NonNull
    private List<City> getInitialCities() {
        //метод возвращает список городов из ресурса приложения Лист
        List<City> cities = new ArrayList<>();
        String[] initialCities = getResources().getStringArray(R.array.initial_cities);
        for (String city : initialCities){
            cities.add(new City(city));
        }
        return cities;
    }

    private void load(List<City> cities, boolean restart){
        for(int i=0; i<cities.size(); i++){
            String cityName = cities.get(i).getName();
            loadWeather(restart, cities.get(i), cityName, i+1);
        }
    }

    private void loadWeather(boolean restart, City city, String cityName, Integer id) {
        mLoadingView.showLoadingIndicator();

        if (restart) {
            // берем данные из Bundle
        } else {
            Call<City> call = App.getConnectionApi().getWeather(cityName);
            call.enqueue(new Callback<City>() {
                @Override
                public void onResponse(Call<City> call, Response<City> response) {
                    showWeather(response.body());
                    mLoadingView.hideLoadingIndicator();
                }
                @Override
                public void onFailure(Call<City> call, Throwable t) {
                    showError();
                    mLoadingView.hideLoadingIndicator();
                }
            });
        }
    }

    private List<City> sortAllcities (List<City> loadedCity) {
        Collections.sort(loadedCity, new Comparator<City>() {
            @Override
            public int compare(City o1, City o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
     return loadedCity;
    }


    private void showWeather (@Nullable City city) {
        if (city==null || city.getName()==null || city.getWeather()==null
                || city.getWind()==null) {
            Log.d(TAG, "city==null = "+city);
            showError();
            return;
        }
        loadedCity.add(city); //ФОРМИРУЕМ СПИСОЧНЫЙ МАССИВ

        if (loadedCity.size() >= cities.size()) {
            mLoadingView.hideLoadingIndicator(); //прячем индикатор загрузки
            sortAllcities(loadedCity); //сортирум по имени
            mAdapter.changeDataSet(loadedCity); //обновляем данные в Recycler View
            loadedCity.clear(); //очищаем массив
        }
    }

    private void showError() {
        mLoadingView.hideLoadingIndicator();
        Snackbar snackbar = Snackbar.make(mRecyclerView, R.string.weather_error, Snackbar.LENGTH_LONG)
                .setAction("Retry", view -> load(cities, true));  //ПЕРЕПИСАЛ БЕЗ ЛЯМБДЫ:
//                .setAction("Retry", new View.OnClickListener() {
//                   @Override
//                   public void onClick(View v) {
//                       load(cities, true);
//                   }
//                });
        snackbar.setDuration(4000);
        snackbar.show();
    }

    @Override
    public void onItemClick(@NonNull City city) {
        Intent intent = new Intent(this, CityActivity.class);
        intent.putExtra(CITY_NAME_KEY, city.getName());
        startActivity(intent);
    }
}

