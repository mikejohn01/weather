package com.mikejohn.weather;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mikejohn.weather.model.City;

import java.util.ArrayList;
import java.util.List;

public class CitiesAdapter extends RecyclerView.Adapter<CityHolder> {

    private final List<City> mCities;
    private final OnItemClick mOnItemClick;

    public CitiesAdapter(@NonNull List<City> cities, @NonNull OnItemClick onItemClick) {
        mCities = new ArrayList<>(cities);
        mOnItemClick = onItemClick;
    }

    public void changeDataSet(@NonNull List<City> cities) {
        mCities.clear();
        mCities.addAll(cities);
        notifyDataSetChanged();
        notifyItemChanged(1);
    }

    @NonNull
    @Override
    public CityHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_city, parent, false);
        return new CityHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CityHolder holder, int position) {
        City city = mCities.get(position);
        holder.bind(city);
        holder.itemView.setTag(city);
        holder.itemView.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return mCities.size();
    }

    public interface OnItemClick {
        void onItemClick(@NonNull City city);
    }

    private final View.OnClickListener onClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            City city = (City) view.getTag();   //получаем шаблон элемента списка
            mOnItemClick.onItemClick(city);     //передаем шаблон обработчику нажатия
        }
    };
}
