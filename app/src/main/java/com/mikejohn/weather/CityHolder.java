package com.mikejohn.weather;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikejohn.weather.model.City;
import com.mikejohn.weather.model.Main;

public class CityHolder extends RecyclerView.ViewHolder {

    public CityHolder(@NonNull View itemView) {
        super(itemView);
    }

    TextView mCityName = itemView.findViewById(R.id.city_name);
    TextView mTemperature = itemView.findViewById(R.id.temperature);

    //String temp;

    public void bind (@NonNull City city) {
        //тут формиируем строку город + температура
        mCityName.setText(city.getName());
        if (city.getMain() != null) {
            String temp = mTemperature.getResources().getString(R.string.temperature, city.getMain().getTemp());
//        } else {
//            temp = "null";
//        }
        mTemperature.setText(temp);
        }
    }
}
