package com.mikejohn.weather.connection;

import android.support.annotation.NonNull;

import com.mikejohn.weather.BuildConfig;

import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import okhttp3.OkHttpClient;

public class App {

    private static volatile ConnectionApi sService;
    private static OkHttpClient sClient;

    private App() {}

    //Метод, возвращающий объект анонимного класса, реализовавшего интерфейса
    @NonNull
    public static ConnectionApi getConnectionApi() {
        ConnectionApi service = sService;   // поле для проверки уникальности запроса
        if (service==null){         //если запроса нет, создаем новый
         synchronized (App.class) { //блок кода будет выполняться только одним потоком
            service = sService;     //создаем экземпляр сервиса
             if (service == null){  //если синхронизированный запрос тоже пустой, т.е. другой поток не записал туда ничего
                 service = sService = buildRetrofit().create(ConnectionApi.class);
             }
         }
        }
        return service;
    }

    @NonNull
    private static Retrofit buildRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    @NonNull
    private static OkHttpClient getClient() {
        OkHttpClient client = sClient;
        if (client == null) {
            synchronized (App.class) {
                client = sClient;
                if (client==null) {
                    client = sClient = buildClient();
                }
            }
        }
        return client;
    }

    @NonNull
    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor())
            //.addInterceptor(new ApiKeyInterceptorJ()
            .addInterceptor(new ApiKeyInterceptor_kt())
            .build();
    }
}

