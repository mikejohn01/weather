package com.mikejohn.weather.connection

import com.mikejohn.weather.BuildConfig
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class ApiKeyInterceptor_kt : Interceptor {
    // interseptor - перехватчик
    override fun intercept (chain: Interceptor.Chain): Response {
        // Intercepter.Chain - возвращает соединение. по которому будет выполнен запрос. Для network уровня вернет connection,
        // для application уровня вернет null
        //в этом переопределенном методе перехватываем наш запрос и добавляем в него ключа аутенфикации
        var request: Request = chain.request()
        val url: HttpUrl = request.url.newBuilder()
                //добавляем строку авторизации:
                .addQueryParameter("appid", BuildConfig.API_KEY)
                .build()
        //снова собираем request:
        request = request.newBuilder().url(url).build()
        return chain.proceed(request)
    }
}