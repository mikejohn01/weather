package com.mikejohn.weather.connection;

import android.support.annotation.NonNull;

import com.mikejohn.weather.model.City;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ConnectionApi {
    //описываем аннотации
    @GET("data/2.5/weather?units=metric")
    Call<City> getWeather(@NonNull @Query("q") String query);
}
