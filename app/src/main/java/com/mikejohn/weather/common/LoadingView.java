package com.mikejohn.weather.common;

public interface LoadingView {

    void showLoadingIndicator();

    void hideLoadingIndicator();
}
