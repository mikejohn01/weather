package com.mikejohn.weather.common;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.mikejohn.weather.R;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

public class LoadingDialog extends DialogFragment {
    //класс фрагметра DialogFragment для показывания окна загрузки
    private static final Handler HANDLER = new Handler(Looper.getMainLooper());

    // реализуем методы показывания диалога загрузки
    // методы показывания сделали в интерфейсе
    @NonNull
    //NOW FragmentManager IS "FINAL" JUST FOR FIX ERROR
    public static LoadingView view(@NonNull final FragmentManager fm) {
    //INVESTIGATE THIS ERROR MORE
        // тут в методе возвращаем экземпляр анонимного класса с реализованными методами интерфейса LoadingView:
        return new LoadingView() {

            private final AtomicBoolean mWaitForHide = new AtomicBoolean();

            @Override
            public void showLoadingIndicator() {
                if (mWaitForHide.compareAndSet(false,true)) {
                    if (fm.findFragmentByTag(LoadingDialog.class.getName())==null) {
                        //если фрагемент еще не создан - создаем его
                        LoadingDialog dialog = new LoadingDialog();
                        dialog.show(fm, LoadingDialog.class.getName());
                    }
                }
            }

            @Override
            public void hideLoadingIndicator() {
                if (mWaitForHide.compareAndSet(true, false)) {
                    //отправляем HideTask на выполнение
                    HANDLER.post(new HideTask(fm));
                }
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, getTheme());
        //чтобы пользователь мог закрыть диалоговое окно
        setCancelable(false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setView(View.inflate(getActivity(), R.layout.dialog_loading, null))
                .create();
    }

    @NonNull
    public static LoadingView view(@NonNull Fragment fragment) {
        // это одноименный метод, который принимает Fragment на тот случай, если будет передаваться Fragment, а не fragmentManager.
        return view(fragment.getFragmentManager());
    }

    private static class HideTask implements Runnable {
        private final Reference<FragmentManager> mFmRef;
        private int mAttempts = 10;

        public HideTask(FragmentManager fm) {
            mFmRef = new WeakReference<>(fm);
        }

        @Override
        public void run (){
            HANDLER.removeCallbacks(this); //удалил сообщения из очереди
            final FragmentManager fm = mFmRef.get();
            if (fm!=null) {      //если есть FragmentManager
                final LoadingDialog dialog = (LoadingDialog) fm.findFragmentByTag(LoadingDialog.class.getName());
                if (dialog!=null) {  //и есть DialogFragment
                    dialog.dismissAllowingStateLoss();   //то мы его прячем
                } else if (--mAttempts >=0) {
                    HANDLER.postDelayed(this, 300); //а если нет, то ждем
                }
            }
        }
    }
}