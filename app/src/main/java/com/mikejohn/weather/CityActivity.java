package com.mikejohn.weather;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.mikejohn.weather.common.LoadingDialog;
import com.mikejohn.weather.common.LoadingView;
import com.mikejohn.weather.connection.App;
import com.mikejohn.weather.model.City;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityActivity extends AppCompatActivity {
    private String cityName;
    private LoadingView loadingView;

    Toolbar toolbar;
    TextView toolbarTitle;
    View cityLayout;
    TextView cityMain;
    TextView temperature;
    TextView pressure;
    TextView humidity;
    TextView wind;
    TextView errorLayout;

    @Override
    protected void onCreate(Bundle savedInstanseState) {
        super.onCreate(savedInstanseState);
        setContentView(R.layout.activity_city);

        toolbar = findViewById(R.id.toolbar_city);
        toolbarTitle = findViewById(R.id.toolbar_title);
        cityLayout = findViewById(R.id.city_layout);
        cityMain = findViewById(R.id.city_main);
        temperature = findViewById(R.id.temperature);
        pressure = findViewById(R.id.pressure);
        humidity = findViewById(R.id.humidity);
        wind = findViewById(R.id.wind);
        errorLayout = findViewById(R.id.error_layout);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {    //если тулбар уже есть обзываем его пустотой
            getSupportActionBar().setTitle("");
        }
        cityName = getIntent().getStringExtra(MainActivity.CITY_NAME_KEY);
        toolbarTitle.setText(cityName);

        loadingView = LoadingDialog.view(getSupportFragmentManager());
        loadWeather(false);
    }

    private void loadWeather(boolean restart) {
        cityLayout.setVisibility(View.INVISIBLE);
        errorLayout.setVisibility(View.GONE);
        loadingView.showLoadingIndicator();

        if (restart) {
            // берем данные из Bundle
        } else {
            Call<com.mikejohn.weather.model.City> call = App.getConnectionApi().getWeather(cityName);
            call.enqueue(new Callback<City>() {
                @Override
                public void onResponse(Call<City> call, Response<City> response) {
                    showWeather(response.body());
                    loadingView.hideLoadingIndicator();
                }

                @Override
                public void onFailure(Call<City> call, Throwable t) {
                    showError();
                    loadingView.hideLoadingIndicator();
                }
            });
        }
    }

    private void showWeather(@Nullable City city) {
        if (city == null || city.getName() == null || city.getWeather() == null
                || city.getWind() == null) {
            showError();
            return;
        }
        loadingView.hideLoadingIndicator();
        errorLayout.setVisibility(View.GONE);
        cityLayout.setVisibility(View.VISIBLE);
        toolbarTitle.setText(city.getName());
        cityMain.setText(city.getWeather().getmMain());
        temperature.setText(getString(R.string.temperature, city.getMain().getTemp()));
        pressure.setText(getString(R.string.pressure, city.getMain().getPressure()));
        humidity.setText(getString(R.string.humidity, city.getMain().getHumidity()));
        wind.setText(getString(R.string.wind, city.getWind().getSpeed()));
    }

    private void showError() {
        loadingView.hideLoadingIndicator();
        cityLayout.setVisibility(View.INVISIBLE);
        errorLayout.setVisibility(View.VISIBLE);
    }
}
