package com.mikejohn.weather.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Main implements Serializable {

    @SerializedName("temp")
    private float temp;

    @SerializedName("pressure")
    private int pressure;

    @SerializedName("humidity")
    private int humidity;

    public float getTemp() { return temp; }

    public int getPressure() { return pressure; }

    public int getHumidity() { return humidity; }
}
