package com.mikejohn.weather.model;

import android.support.annotation.Nullable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

public class City implements Serializable {

    public City() {}

    public City (@NonNull String name){
        mName = name;
    }

    @SerializedName("name")
    private String mName;

    @SerializedName("weather")
    private List<Weather> mWeathers;

    @SerializedName("main")
    private Main mMain;

    @SerializedName("wind")
    private Wind mWind;

    @NonNull
    public String getName(){
        return mName;
    }

    public void setName (@NonNull String name){
        mName = name;
    }

    @Nullable
    public Weather getWeather(){
        if(mWeathers==null || mWeathers.isEmpty()) {
            return null;
        }
        return mWeathers.get(0);
    }

    @Nullable
    public Main getMain(){
        return mMain;
    }

    @Nullable
    public Wind getWind(){
        return mWind;
    }
}
